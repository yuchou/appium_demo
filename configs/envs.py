desired_caps = {
    'platformName': 'Android',
    'platformversion': '8.1.0',
    'deviceName': 'bullhead model:Nexus_5X',
    'appPackage': 'com.klook',
    'appActivity': 'com.klooklib.activity.NormalIconActivity',
    'unicodeKeyboard': True,
    'resetKeyboard': True
}

options = {
    "appium_url": "http://localhost:4723/wd/hub"
}
