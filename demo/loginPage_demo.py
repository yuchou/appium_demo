import time
from pages.basePage import BasePage


class LoginPage(BasePage):

    _login_tap = 'new UiSelector().resourceId("com.klook:id/shifting_bottom_navigation_container").instance(4)'
    _login_area = 'new UiSelector().resourceId("com.klook:id/user_fragment_tvlogin")'
    _login_btn = 'new UiSelector().resourceId("com.klook:id/login_bt")'
    _email_input = 'new UiSelector().resourceId("com.klook:id/email_et")'
    _passwd_input = 'new UiSelector().resourceId("com.klook:id/password_et")'
    _login_btn2 = 'new UiSelector().resourceId("com.klook:id/login_button")'
    
    def login_tap(self):
        self.driver.find_element_by_android_uiautomator(self._login_tap).click()
    
    def login_area_tap(self):
        self.driver.find_element_by_android_uiautomator(self._login_area).click()
        time.sleep(1)
        self.driver.find_element_by_android_uiautomator(self._login_btn).click()
    
    def login_with_email(self, email, password):
        self.driver.find_element_by_android_uiautomator(self._email_input).set_value(email)
        time.sleep(1)
        self.driver.find_element_by_android_uiautomator(self._passwd_input).set_value(password)
        time.sleep(1)
        self.driver.find_element_by_android_uiautomator(self._login_btn2).click()
