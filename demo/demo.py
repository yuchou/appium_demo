#!/usr/bin/env python
# coding: utf-8
import time
from appium import webdriver


def test_klook(appium_driver):
    '''测试登陆'''
    el = appium_driver.find_element_by_android_uiautomator('new UiSelector().resourceId("com.klook:id/shifting_bottom_navigation_container").instance(4)')
    el.click()


def test_failed(appium_driver):
    '''测试失败'''
    el = appium_driver.find_element_by_android_uiautomator('new UiSelector().resourceId("test")')
    el.click()