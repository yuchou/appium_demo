#!/usr/bin/env python
# coding: utf-8
import os
import pytest
from py.xml import html
from appium import webdriver
from configs.envs import desired_caps, options


def pytest_configure(config):
    if hasattr(config, 'slaveinput'):
        return
    
    ensure_dir('reports')

    if config.pluginmanager.hasplugin('html'):
        from utils.html_reporting import AppiumReportPlugin
        config.pluginmanager.register(AppiumReportPlugin())


def pytest_addhooks(pluginmanager):
    from utils import hooks
    method = getattr(pluginmanager, 'add_hookspecs', None)
    if method is None:
        method = pluginmanager.addhooks
    method(hooks)


def pytest_addoption(parser):
    _capture_choices = ('never', 'failure', 'always')
    parser.addini(
        'appium_capture_debug',
        help='when debug is captured {0}'.format(_capture_choices),
        default=os.getenv('APPIUM_CAPTURE_DEBUG', 'failure'),
    )
    parser.addini(
        'appium_exclude_debug',
        help='debug to exclude from capture',
        default=os.getenv('APPIUM_EXCLUDE_DEBUG'),
    )


@pytest.yield_fixture
def driver(request):
    '''Provide test appium driver'''
    driver = webdriver.Remote(options['appium_url'], desired_caps)
    driver.implicitly_wait(30)

    request.node._driver = driver
    yield driver
    driver.quit()


@pytest.yield_fixture(scope='session')
def driver_session_(request):
    yield from driver(request)


@pytest.yield_fixture
def driver_session(request, driver_session_):
    """
    Appium Session
    """
    request.node._driver = driver_session_  # Required to facilitate screenshots in html reports
    yield driver_session_


@pytest.yield_fixture
def appium(driver_session):
    """Alias for driver_session"""
    yield driver_session


@pytest.fixture
def appium_driver(appium):
    return appium


@pytest.mark.optionalhook
def pytest_html_results_table_header(cells):
    '''Add test functions desc'''
    cells.insert(1, html.th('Description'))


@pytest.mark.optionalhook
def pytest_html_results_table_row(report, cells):
    '''Add test functions desc'''
    cells.insert(1, html.td(report.description))


def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
