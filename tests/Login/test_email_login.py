from pages.Login.loginPage import LoginPage


def test_login(appium_driver):
    '''登陆操作'''
    page = LoginPage(appium_driver)
    page.login_tap()
    page.login_area_tap()
    page.login_with_email(email='xxx@qq.com', password='xxxx')