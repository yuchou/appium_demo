from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import os
from selenium.common.exceptions import NoSuchElementException, TimeoutException
import time
import logging


class BasePage(object):
    def __init__(self, appium_driver):
        self.driver = appium_driver
        self.X = self.driver.get_window_size()['width']
        self.Y = self.driver.get_window_size()['height']

    def find_element(self, *loc):
        try:
            # 元素可见时，返回查找到的元素；以下入参为元组的元素，需要加*
            WebDriverWait(self.driver, 30).until(
                lambda driver: driver.find_element(*loc).is_displayed())
            return self.driver.find_element(*loc)

        except NoSuchElementException:
            logging.warning('Can not find element: %s' % loc[1])
            raise
        except TimeoutException:
            raise

    def find_elements(self, *loc):
        try:
            WebDriverWait(self.driver,
                          30).until(lambda driver: driver.find_elements(*loc))
            return self.driver.find_elements(*loc)

        except NoSuchElementException:
            logging.warning('Can not find element: %s' % loc[1])
            raise

    def find_element_ui(self, *loc):
        pass

    def find_elements_ui(self, *loc):
        pass

    def click(self, loc):
        logging.debug('Click element by %s: %s...' % (loc[0], loc[1]))
        try:
            self.find_element(*loc).click()
            time.sleep(1)
        except AttributeError:
            raise

    def double_click(self, loc):
        logging.debug('Click element by %s: %s...' % (loc[0], loc[1]))
        try:
            self.find_element(*loc).click()
            self.find_element(*loc).click()
        except AttributeError:
            raise

    def clicks(self, loc, index):
        logging.debug('Click element by %s: %s...' % (loc[0], loc[1]))
        try:
            self.find_elements(*loc)[index].click()
            time.sleep(1)
        except AttributeError:
            raise

    def click_back_key(self):
        logging.debug('Click device back key...')
        self.driver.keyevent(4)
        time.sleep(1)

    def send_key(self, loc, text):
        try:
            logging.debug('Clear input-box: %s...' % loc[1])
            self.find_element(*loc).clear()
            time.sleep(1)

            logging.debug('Input: %s' % text)
            self.find_element(*loc).send_keys(text)
            self.hide_keyboard()
            time.sleep(1)
        except TimeoutException:
            raise
        except Exception:
            pass

    def send_keys(self, loc, index, text):
        try:
            logging.debug('Clear input-box: %s...' % loc[1])
            self.find_elements(*loc)[index].clear()
            time.sleep(1)

            logging.debug('Input: %s' % text)
            self.find_elements(*loc)[index].send_keys(text)
            time.sleep(1)
        except AttributeError:
            raise

    def hide_keyboard(self):
        self.driver.hide_keyboard()

    def is_display(self, loc):
        try:
            WebDriverWait(self.driver, 10).until(
                lambda driver: driver.find_element(*loc).is_displayed())
            return True
        except:
            return False

    def get_windowsize(self):
        '''获取屏幕的高度和宽度'''
        height = self.driver.get_window_size()['height']
        width = self.driver.get_window_size()['width']
        return height, width

    def swipe_up(self):
        '''向上滑动屏幕'''
        height, width = self.get_windowsize()
        self.driver.swipe(width / 2, height * 3 / 4, width / 2, height * 1 / 4)

    def swipe_down(self):
        '''向下滑动屏幕'''
        height, width = self.get_windowsize()
        self.driver.swipe(width / 2, height * 1 / 4, width / 2, height * 3 / 4)

    def swipe_left(self):
        '''向左滑动屏幕'''
        height, width = self.get_windowsize()
        self.driver.swipe(width * 3 / 4, height / 2, width * 1 / 4, height / 2)

    def swipe_right(self):
        '''向右滑动屏幕'''
        height, width = self.get_windowsize()
        self.driver.swipe(width * 1 / 4, height / 2, width * 3 / 4, height / 2)

    # 获取toast信息，报错
    # def find_toast(self, message):
    #     # mes = '//*[@text="请输入账号"]'
    #     elem = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(By.XPATH, message))
    #     print elem

    def get_current_activity_name(self):
        '''获取当前activity的名称'''
        activity_name = self.driver.current_activity
        logging.debug('Current activity name is: %s' % activity_name)
        return activity_name

    def is_login(self):
        pass
