from selenium.webdriver.common.by import By
from pages.basePage import BasePage


class LoginPage(BasePage):

    _login_tap = (By.ID, "shifting_bottom_navigation_container")
    _login_area = (By.ID, "user_fragment_tvlogin")
    _login_btn = (By.ID, "login_bt")
    _email_input = (By.ID, "email_et")
    _passwd_input = (By.ID, "password_et")
    _login_btn2 = (By.ID, "login_button")
    
    def login_tap(self):
        self.clicks(self._login_tap, 4)
    
    def login_area_tap(self):
        self.click(self._login_area)
        self.click(self._login_btn)
    
    def login_with_email(self, email, password):
        self.send_key(self._email_input, email)
        self.send_key(self._passwd_input, password)
        self.click(self._login_btn2)
